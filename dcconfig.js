// Import styles (automatically injected into <head>).
//import "common.css";


var wifiCreds;
var	ble_cmd_locate = undefined;
var	ble_cmd_reboot = null;
var	ble_cmd_runble = null;

const infoItemsDisplayNames = {
	"device_id"	: "Device Id",
	"model" 	: "Model",
	"wifi_ap"	: "Wifi AP",
	"wifi_ip"	: "Ip Address",
	"cpu_temp"	: "CPU Temperature",
	"cpu_load"	: "CPU Load",
	"cpu_usage"	: "CPU Usage",
	"memory"	: "Memory",
	"uptime"	: "Up time",
	"battery"	: "Battery",
}

window.onload = function () {
	console.log(`Hello World! ${window.innerWidth}x${window.innerHeight}`);
	setOrientation();
	window.addEventListener( 'resize', setOrientation);

	try {
	  navigator.bluetooth.getAvailability()
	  .then(isBluetoothAvailable => {
	    console.log(`> Bluetooth is ${isBluetoothAvailable ? 'available' : 'unavailable'}`);
		document.getElementById("notAvailableHolder").textContent = '';
		document.getElementById("notAvailableHolder").style.background = 'dodgerblue';
		const button = document.createElement('button');
		button.className = "discoverButton";
		button.innerText = "Discover Device";
		button.addEventListener('click', onDeviceSearch);
		document.getElementById("notAvailableHolder").appendChild(button);
	  });
	} catch (error) {
		const available = document.getElementById("notAvailableHolder");
		available.textContent = 'Bluetooth not supported by this browser!';
		available.style.background = 'DodgerBlue';
		available.font = 'bold';

		window.stop();
		var id = window.setTimeout(function() {}, 0);
		while (id--) {
		  window.clearTimeout(id);
		}
	}

	function onDisconnected(event) {
		document.getElementById("devicePanelsHolder").style.display = 'none';
		document.getElementById("notAvailableHolder").style.display = 'flex';
		const device = event.target;
		console.log(`Device ${device.name} is disconnected.`);
		// Reset variables
		ble_cmd_locate = undefined;
	}


	function onDeviceSearch() {
	  let filters = [];
	  let options = {};
	  filters.push({services: [uuids.SERVICE_ID]});
	  options.filters = filters;
	  //options.acceptAllDevices = true;
	  options.optionalServices = [uuids.SERVICE_ID];

	  console.log('Requesting Bluetooth Device...');
	  console.log('with ' + JSON.stringify(options));
	  navigator.bluetooth.requestDevice(options)
	  .then(device => {
		console.log('> Name:             ' + device.name);
		console.log('> Id:               ' + device.id);
		console.log('> Connected:        ' + device.gatt.connected);

		document.getElementById("notAvailableHolder").style.display = 'none';
		document.getElementById("devicePanelsHolder").style.display = 'flex';
		const titleHolder = document.getElementById("titleHolder");
		titleHolder.style.borderBottomLeftRadius = "6px";
		titleHolder.style.borderBottomRightRadius = "6px";

		// Create panel for Device Information
		const infoPanel = document.getElementById("infoPanel");
		while (infoPanel.lastChild) infoPanel.removeChild(infoPanel.lastChild);
		infoPanel.insertAdjacentHTML('afterbegin', '<div id="infoLabel" class="panel-label">Device Information</div>');
		var elements = document.getElementsByClassName('panel');
		for (let element of elements) { element.style.background = "silver"; }

		// Create panel for Wifi
		const wifiPanel = document.getElementById("wifiPanel");
		while (wifiPanel.lastChild) wifiPanel.removeChild(wifiPanel.lastChild);
		wifiPanel.insertAdjacentHTML('afterbegin', '<div id="wifiPanel" class="panel-label">Wifi Setting</div>');

		// Create panel for Custom Commands
		const commandPanel = document.getElementById("commandPanel");
		while (commandPanel.lastChild) commandPanel.removeChild(commandPanel.lastChild);
		commandPanel.insertAdjacentHTML('afterbegin', '<div id="commandPanel" class="panel-label">Device Commands</div>');

		if (!device.gatt.connected) {
			device.addEventListener('gattserverdisconnected', onDisconnected);
			return device.gatt.connect();
		}
	  })
	  .then(server => {
		console.log('Obtaining Primary Service');
		return server.getPrimaryService(uuids.SERVICE_ID);
	  })
	  .then((service) => {
		console.log('Obtaining Characteristics');
		return service.getCharacteristics();
	  })
	  .then((characteristics) => {
		var info_characteristic;
		var info_items = [];
		characteristics.forEach( (characteristic) => {
			switch (characteristic.uuid) {
				case uuids.DEVICE_INFO:
					//	This is the characteristic with which to request device info
					info_characteristic = characteristic;
					break;
				case uuids.CMD_LOCATE:
					ble_cmd_locate = characteristic;
					break;
				case uuids.CMD_REBOOT:
					ble_cmd_reboot = characteristic;
					break;
				case uuids.CMD_RUNBLE:
					ble_cmd_runble = characteristic;
					break;
				case uuids.WIFI_CREDS:
					wifiCreds = characteristic;
					break;
				case uuids.INFO_DEVICE_ID:
					ble_info_id = characteristic;
					ble_info_id.startNotifications().then(_ => {
						characteristic.addEventListener('characteristicvaluechanged', handleInfoDevice_IdUpdate);
					});
					info_items.push("device_id");
					break;
				case uuids.REPORTS_ID:
					ble_reports_id = characteristic;
					ble_reports_id.startNotifications().then(_ => {
						characteristic.addEventListener('characteristicvaluechanged', handleReports_IdUpdate);
					});
					//info_items.push("reports_id");
					break;
				case uuids.INFO_MODEL:
					ble_info_model = characteristic;
					ble_info_model.startNotifications().then(_ => {
						ble_info_model.addEventListener('characteristicvaluechanged', handleInfoModelUpdate);
					});
					info_items.push("model");
					break;
				case uuids.INFO_WIFI_AP:
					ble_info_wifi_ap = characteristic;
					ble_info_wifi_ap.startNotifications().then(_ => {
						ble_info_wifi_ap.addEventListener('characteristicvaluechanged', handleInfoWifiApUpdate);
					});
					info_items.push("wifi_ap")
					break;
				case uuids.INFO_WIFI_IP:
					ble_info_wifi_ip = characteristic;
					ble_info_wifi_ip.startNotifications().then(_ => {
						ble_info_wifi_ip.addEventListener('characteristicvaluechanged', handleInfoWifiIpUpdate);
					});
					info_items.push("wifi_ip")
					break;
				case uuids.INFO_CPU_TEMP:
					ble_info_cpu_temp = characteristic;
					ble_info_cpu_temp.startNotifications().then(_ => {
						ble_info_cpu_temp.addEventListener('characteristicvaluechanged', handleInfoCpuTempUpdate);
					})
					info_items.push("cpu_temp")
					break;
				case uuids.INFO_CPU_LOAD:
					ble_info_cpu_load = characteristic;
					ble_info_cpu_load.startNotifications().then(_ => {
						ble_info_cpu_load.addEventListener('characteristicvaluechanged', handleInfoCpuLoadUpdate);
					})
					info_items.push("cpu_load")
					break;
				case uuids.INFO_CPU_USAGE:
					ble_info_cpu_usage = characteristic;
					ble_info_cpu_usage.startNotifications().then(_ => {
						ble_info_cpu_usage.addEventListener('characteristicvaluechanged', handleInfoCpuUsageUpdate);
					})
					info_items.push("cpu_usage")
					break;
				case uuids.INFO_MEMORY:
					ble_info_memory = characteristic;
					ble_info_memory.startNotifications().then(_ => {
						ble_info_memory.addEventListener('characteristicvaluechanged', handleInfoMemoryUpdate);
					})
					info_items.push("memory")
					break;
				case uuids.INFO_UPTIME:
					ble_info_uptime = characteristic;
					ble_info_uptime.startNotifications().then(_ => {
						ble_info_uptime.addEventListener('characteristicvaluechanged', handleInfoUptimeUpdate);
					})
					info_items.push("uptime")
					break;
				case uuids.INFO_BATTERY:
					ble_info_battery = characteristic;
					ble_info_battery.startNotifications().then(_ => {
						ble_info_battery.addEventListener('characteristicvaluechanged', handleInfoBatteryUpdate);
					})
					info_items.push("battery")
					break;
				default:
					console.log(`Found unprocessed characteristic: ${characteristic.uuid}`);
			}
		});
		info_items.forEach( (item) => {
			if (item == "cpu_temp")
				addInfoItem(item, "53.33");
			else
				addInfoItem(item, "....");
		});
		setup_wifi_panel();
		addCommandItems();
		if (info_items.length > 0) {
			info_characteristic.writeValue(new TextEncoder().encode(info_items.toString()));
		}
	  });
	}

	function setOrientation () {
		if (window.innerWidth < (2 * window.innerHeight / 3)) {
			document.getElementById("devicePanelsHolder").style.flexDirection = 'column';
			document.getElementById("devicePanelsHolder").style.alignItems = 'center';
			document.getElementById("devicePanelsHolder").style.width = '96%';
		}
		else {
			document.getElementById("devicePanelsHolder").style.flexDirection = 'row';
			document.getElementById("devicePanelsHolder").style.alignItems = 'center';
			document.getElementById("devicePanelsHolder").style.width = '100%';
		}
	};

}

//	Add a group for Device Information display.
//	Each group has a display label (based on the item name)
//	and a default value to display
function addInfoItem (item, value) {
	//console.log(`item: ${item}`);
	//console.log(`value: ${value}`);
	//	Check whether we will handle this item.
	if (! (item in infoItemsDisplayNames)) {
		console.log(`Can't set up unknown item: ${item}`);
		return;
	}
	const infoPanel = document.getElementById("infoPanel");
	const infoGroup = document.createElement('div'); infoGroup.id = `info-group-${item}`; infoGroup.className = "info-group";
	const groupLabel = document.createElement('div'); groupLabel.className = "label";
	const groupValue = document.createElement('div'); groupValue.id = `info-value-${item}`; groupValue.className = "value";
	//groupLabel.textContent = label;
	groupLabel.textContent = infoItemsDisplayNames[item];
	if (value) {
		groupValue.textContent = value;
	}
	else {
		groupValue.textContent = "....";
	}
	infoGroup.appendChild(groupLabel);
	infoGroup.appendChild(groupValue);
	infoPanel.appendChild(infoGroup);
}


function handleInfoDevice_IdUpdate (event) {
	//console.log("handleInfoDevice_IdUpdate()");
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	document.getElementById(`info-value-device_id`).textContent = str;
}
function handleReports_IdUpdate (event) {
	//console.log(`handleReports_IdUpdate()`);
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	console.log(`handleReports_IdUpdate(): ${str}`);
	try {
		const msg = JSON.parse(str);
		if (Object.keys(msg).includes("msg_type")) {
			if (msg.msg_type == "disconnect")
				location.reload();
			else if (msg.msg_type == "badcreds")
				alert("No network found with that SSID/password combination");
			else
				console.log("Unknown report")
		}
	}
	catch (err) {
		console.log(`ERROR in handleReports_IdUpdate(): ${err}`);
	}
}
function handleInfoModelUpdate (event) {
	//console.log("handleInfoModelUpdate()");
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	document.getElementById(`info-value-model`).textContent = str;
}
function handleInfoWifiApUpdate (event) {
	//console.log("handleInfoWifiApUpdate()");
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	document.getElementById(`info-value-wifi_ap`).textContent = str;
}
function handleInfoWifiIpUpdate (event) {
	//console.log("handleInfoWifiIpUpdate()");
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	document.getElementById(`info-value-wifi_ip`).textContent = str;
}
function handleInfoCpuTempUpdate (event) {
	//console.log("handleInfoCpuTempUpdate()");
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	if (str == "53.33") return;
	document.getElementById(`info-value-cpu_temp`).textContent = str;
}
function handleInfoCpuLoadUpdate (event) {
	//console.log("handleInfoCpuLoadUpdate()");
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	document.getElementById(`info-value-cpu_load`).textContent = str;
}
function handleInfoCpuUsageUpdate (event) {
	//console.log("handleInfoCpuUsageUpdate()");
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	document.getElementById(`info-value-cpu_usage`).textContent = str;
}
function handleInfoMemoryUpdate (event) {
	//console.log("handleInfoMemoryUpdate()");
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	document.getElementById(`info-value-memory`).textContent = str;
}
function handleInfoUptimeUpdate (event) {
	//console.log("handleInfoUptimeUpdate()");
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	uptime_str = format_uptime(parseInt(str));
	document.getElementById(`info-value-uptime`).textContent = uptime_str;
}
function handleInfoBatteryUpdate (event) {
	//console.log("handleInfoBatteryUpdate()");
	let value = event.target.value;
	const str = new TextDecoder("utf-8").decode(value.buffer);
	document.getElementById(`info-value-battery`).textContent = str;
}


function addCommandItems () {
	console.log(`addCommandItems()`);
	const commandPanel = document.getElementById("commandPanel");

	if (! document.getElementById("commandItemsHolder")) {
		const commandItemsHolder = document.createElement('div'); commandItemsHolder.id = "commandItemsHolder"; commandItemsHolder.className = "commandItemsHolder";
		commandPanel.appendChild(commandItemsHolder);
	}

	//	REBOOT command is compulsory
	const rebootButtonHolder = document.createElement('div');
		rebootButtonHolder.className.id = "rebootButtonHolder";
		rebootButtonHolder.className = "rebootButtonHolder commandButtonHolder";
	const commandRebootButton = document.createElement('button');
		commandRebootButton.id = "commandRebootButton";
		commandRebootButton.className = "commandButton";
		commandRebootButton.innerText = "REBOOT device";
		commandRebootButton.addEventListener('click', () => {
			console.log("on reboot button");
			if (!ble_cmd_reboot) {
				console.log(`ble_cmd_reboot characteristic not found yet`);
				return;
			}
			if (confirm("Are you sure? Clicking OK will reboot the connected device ...")) {
				ble_cmd_reboot.writeValue(new TextEncoder().encode("reboot")).then(_ => {
					console.log(`reboot command written`);
				})
				.catch((error) => {
					console.log(`More Argh! ${error}`);
				});
			}
		});
	rebootButtonHolder.appendChild(commandRebootButton);
	commandItemsHolder.appendChild(rebootButtonHolder);

	//	STOP BLE is compulsory
	const stopBleButtonHolder = document.createElement('div');
		stopBleButtonHolder.className.id = "stopBleButtonHolder";
		stopBleButtonHolder.className = "stopBleButtonHolder commandButtonHolder";
	const commandStopBleButton = document.createElement('button');
		commandStopBleButton.id = "commandStopBleButton";
		commandStopBleButton.className = "commandButton";
		commandStopBleButton.innerText = "Stop BLE";
		commandStopBleButton.addEventListener('click', () => {
			console.log("on stopBle button");
			if (!ble_cmd_runble) {
				console.log(`ble_cmd_runble characteristic not found yet`);
				return;
			}
			if (confirm("Are you sure? Clicking OK will close the Bluetooth connection (although the device will continue running)")) {
				ble_cmd_runble.writeValue(new TextEncoder().encode("stop_ble")).then(_ => {
					console.log(`stopBle command written`);
				})
				.catch((error) => {
					console.log(`More Argh! ${error}`);
				});
			}
		});
	stopBleButtonHolder.appendChild(commandStopBleButton);
	commandItemsHolder.appendChild(stopBleButtonHolder);

	//	TEST LED is optional - some devices may not have an LED to flash
	if (ble_cmd_locate) {
		//console.log(`have ble_cmd_locate ${typeof ble_cmd_locate}`);

		const testLedButtonHolder = document.createElement('div');
			testLedButtonHolder.className.id = "testLedButtonHolder";
			testLedButtonHolder.className = "testLedButtonHolder commandButtonHolder";
		const commandTestLedButton = document.createElement('button');
			commandTestLedButton.id = "commandTestLedButton";
			commandTestLedButton.className = "commandButton";
			commandTestLedButton.innerText = "Locate device (flash LED)";
			commandTestLedButton.addEventListener('click', () => {
				ble_cmd_locate.writeValue(new TextEncoder().encode(bleVal.CMD_LOCATE_TOGGLE)).then(_ => {
					//console.log(`testLed command written`);
				})
				.catch((error) => {
					console.log(`More Argh! ${error}`);
				});
			});
		testLedButtonHolder.appendChild(commandTestLedButton);
		commandItemsHolder.appendChild(testLedButtonHolder);
	}

}

function setup_wifi_panel () {
	console.log(`setup_wifi_panel()`);
	const wifiPanel = document.getElementById("wifiPanel");
	const wifiItemsHolder = document.createElement('div'); wifiItemsHolder.id = "wifiItemsHolder"; wifiItemsHolder.className = "wifiItemsHolder";
	wifiPanel.appendChild(wifiItemsHolder);

	const ssidHolder = document.createElement('div'); ssidHolder.className = "ssidHolder credHolder";
	const ssidLabel = document.createElement('label'); ssidLabel.textContent = "Wifi name (SSID)"; ssidLabel.for = "ssidInput";
	const ssidInput = document.createElement('input'); ssidInput.id = "ssidInput"; ssidInput.type = "text";
	ssidHolder.appendChild(ssidLabel); ssidHolder.appendChild(ssidInput); 
	ssidInput.oninput = function () {
		const wifiSendButton = document.getElementById("wifiSendButton")
		if (ssidInput.value.length < 1)
			wifiSendButton.setAttribute("disabled", "disabled");
		else
			wifiSendButton.removeAttribute("disabled");
	}

	const passwdHolder = document.createElement('div'); passwdHolder.className = "passwdHolder credHolder";
	const passwordLabel = document.createElement('label'); passwordLabel.textContent = "Wifi password"; passwordLabel.for = "passwordInput";
	const passwordInput = document.createElement('input'); passwordInput.id = "passwordInput"; passwordInput.type = "password";
	passwordLabel.addEventListener('click', () => {
		const passwordInput = document.getElementById("passwordInput");
		if (passwordInput.type == "password") {
			passwordInput.type = "text";
		}
		else passwordInput.type = "password";
	});
	passwdHolder.appendChild(passwordLabel); passwdHolder.appendChild(passwordInput); 

	const keyHolder = document.createElement('div'); keyHolder.className = "keyHolder credHolder";
	const keyLabel = document.createElement('label'); keyLabel.textContent = "Key"; keyLabel.for = "keyInput";
	const keyInput = document.createElement('input'); keyInput.id = "keyInput"; keyInput.type = "text";
	keyHolder.appendChild(keyLabel); keyHolder.appendChild(keyInput); 

	const sendButtonHolder = document.createElement('div');
		sendButtonHolder.className.id = "sendButtonHolder";
		sendButtonHolder.className = "sendButtonHolder credHolder";
	const wifiSendButton = document.createElement('button');
		wifiSendButton.id = "wifiSendButton";
		wifiSendButton.className = "wifiSendButton";
		wifiSendButton.innerText = "SEND new Wifi settings";
		wifiSendButton.setAttribute("disabled", "disabled");
		wifiSendButton.addEventListener('click', onSendWifiSettings);
	sendButtonHolder.appendChild(wifiSendButton);

	wifiItemsHolder.appendChild(ssidHolder);
	wifiItemsHolder.appendChild(passwdHolder);
	wifiItemsHolder.appendChild(keyHolder);
	wifiItemsHolder.appendChild(sendButtonHolder);
	return wifiCreds.readValue()
		.then(value => {
			const max_length = parseInt(new TextDecoder().decode(value));
			//console.log(`WIFI_CREDS_MAX_LENGTH = ${max_length}, ${typeof max_length}`);
			document.getElementById("ssidInput").maxLength = max_length;
			document.getElementById("passwordInput").maxLength = max_length;
		})
		.catch((err) => {
			console.log(`wifiCreds.readValue() failed: ${err}`);
			document.getElementById("ssidInput").maxLength = parseInt(bleVal.WIFI_CREDS_MAX_LENGTH);
			document.getElementById("passwordInput").maxLength = parseInt(bleVal.WIFI_CREDS_MAX_LENGTH);
		})

	function onSendWifiSettings (event) {
		if (!wifiCreds) {
			console.log(`wifiCreds characteristic not found yet`);
			return;
		}
		console.log("onSendWifiSettings()");
		const ssidInput = document.getElementById("ssidInput");
		const passwordInput = document.getElementById("passwordInput");

		// bleVal.WIFI_CREDS_MAX_LENGTH should be set to value retrieved from server (not working yet for Rpi server)
		const message = `${ssidInput.value.padEnd(bleVal.WIFI_CREDS_MAX_LENGTH)}${passwordInput.value.padEnd(bleVal.WIFI_CREDS_MAX_LENGTH)}`;
		if (confirm("Are you sure? Clicking OK will modify this device to use the new SSID & password ...")) {
			wifiCreds.writeValue(new TextEncoder().encode(message)).then(_ => {
				ssidInput.value = "";
				passwordInput.value = "";
				document.getElementById("wifiSendButton").setAttribute("disabled", "disabled");
			})
			.catch((error) => {
				console.log(`More Argh! ${error}`);
				alert("Wifi Setting failed. Check SSID and password");
			});
		}
	}

}

/*	Return timestamp (in microseconds) as formatted string:
		days.hrs:mins
*/
function format_uptime (timestamp) {
	//console.log(`timestamp = ${timestamp}`);
	const mins  = Math.floor((timestamp / (1000000 * 60)) % 60);
	const hours = Math.floor((timestamp / (1000000 * 60 * 60)) % 24);
	const days  = Math.floor((timestamp / (1000000 * 60 * 60 * 24)) % 365);
	if ((days < 1) && (hours < 1)) {
		return `${mins}  minutes`;
	}
	else if (days < 1) {
		return `${hours} hrs, ${mins} mins`;
	}
	else if (days == 1) {
		return `${days}day, ${hours}hrs, ${mins}mins`;
	}
	else return `${days}days, ${hours}hrs, ${mins}mins`;
}

