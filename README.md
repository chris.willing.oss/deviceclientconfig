# DeviceClient Config

A Bluetoth Low Energy (BLE) based configuration tool for small devices

## Description

When deploying  multiple devices which are intended to connect wirelessly to the Internet (or even just a local network), it is difficult to hard-wire any particular configuration into the device which will work in all situations. In particular, the device will need to know an appropriate wireless network Access Point, along with a password to join the network.

This project provides a web-bluetooth service which detects appropriately primed nearby devices and enables an SSID and password to be conveyed to the device, enabling it to join the network specified by the supplied SSID & password. It is assumed that the network's DHCP system will then provide an IP address for the device.

Details about priming peripheral devices to provide a suitable configuration service are available for:
- [ESP32 devices](https://gitlab.com/chris.willing.oss/DeviceClient/-/tree/main/DeviceClientBLE_Configure)
- [Raspberry Pi Zero W](https://gitlab.com/chris.willing.oss/DeviceClient/-/tree/main/DeviceClientBLE_Configure_PiZeroW) (probably Raspberry Pi devices in general)


## Installation

The browser based version of this project requires a browser which implements the Web Bluetooth API. [Please visit here](https://github.com/WebBluetoothCG/web-bluetooth/blob/main/implementation-status.md) to check Web Bluetooth compatibility.

Download the contents of this directory `git clone https://gitlab.com/chris.willing.oss/deviceclientconfig.git` and make them available to a web server.

Alternatively, after downloading, point a WEB Bluetooth API conforming browser at the downloaded index.html file (like the example pix below), 

## Usage

When first run, the browser should display just a _Discover Device_ button
![discover](pix/01_small_discover.png)

Pressing the  _Discover Device_ button starts scanning for devices implementing the configuration service.
![](pix/02_small_scan.png)

After selecting a device and pressing the _Pair_ button, the full interface to the device is displayed. Notice that _Wifi AP_ field in the _Device Information_ panel is empty and no _IP Address_ is available. This is consistent with a device running for the first time, therefore without any wifi configuration applied. 
![](pix/03_small_raw.png)

On entering SSID and password fields, the previously inactive _SEND new Wifi settings_ button becomes available.
![](pix/04_small_setwifi.png)

After pressing the _SEND new Wifi settings_ button, the _Wifi AP_ field now shows the configured access poing SSID. Depending on the device, the _IP Address_ field may or may not already show the new IP address. Devices in the Raspberry Pi family will already display the new IP address and be able to access the network. ESP32 devices like this one require a (1 second) reboot to establish network connectivity.
![](pix/05_small_setwifi2.png)

For ESP32 devices, pressing the _REBOOT device_ button instigates a confirmation dialog.
![](pix/06_small_reboot.png)

After reboot (not required for Raspberry Pi family) the _IP Address_ field is now populated.
![](pix/07_small_result.png)

When configuration is complete, the configuration service can be terminated by pressing _Stop BLE_ button in the _Device Commands_ panel. Although this terminates the device's configuration service, other Bluetooth services should not be affected. The configuration service may be restarted by rebooting the device. Of course, if desired, the configuration service may be left running.

If the _Locate device_ toggle button is present, it indicates that the device has an onboard LED available to be flashed when the _Locate device_ button pressed. This is helpful to indicate which device is currently connected when a number of devices may be present.


## Support

Please report any problems through the [Issues page](https://gitlab.com/chris.willing.oss/deviceclientconfig/-/issues).


## Roadmap

Next to do is creation of a stand alone application so that a Bluetooth enabled browser is not required.


## Contributing

Contributions are welcome either as [Merge Requests](https://gitlab.com/chris.willing.oss/deviceclientconfig/-/merge_requests) or comments to the [DeviceClient Config Issues](https://gitlab.com/chris.willing.oss/deviceclientconfig/-/issues) page.


## License

MIT (http://opensource.org/licenses/MIT)
