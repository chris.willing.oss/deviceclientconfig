// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

uuids =	{
	SERVICE_ID: "9f9c635f-3321-4490-9bae-c58572125000",		// Main service
	REPORTS_ID: "9f9c635f-3321-4490-9bae-c58572125001",		// Regular notified device info reports
	DEVICE_INFO: "9f9c635f-3321-4490-9bae-c58572125002",	// Specific request for device info
	WIFI_CREDS: "9f9c635f-3321-4490-9bae-c58572125003",		// Provide ssid,passwd,key 
	COMMANDS: "9f9c635f-3321-4490-9bae-c58572125004",		// Send commands to the device
	CMD_LOCATE: "9f9c635f-3321-4490-9bae-c58572125005",		// Handle the locate function (flash LED)
	CMD_REBOOT: "9f9c635f-3321-4490-9bae-c58572125006",		// Handle reboot of device
	CMD_RUNBLE: "9f9c635f-3321-4490-9bae-c58572125007",		// Whether to stop device BLE
	INFO_DEVICE_ID: "9f9c635f-3321-4490-9bae-c58572125008",	// Device ID
	INFO_MODEL: "9f9c635f-3321-4490-9bae-c58572125009",		// Device Model
	INFO_WIFI_AP: "9f9c635f-3321-4490-9bae-c58572125010",	// SSID to which device is attached
	INFO_WIFI_IP: "9f9c635f-3321-4490-9bae-c58572125011",	// IP address to which device is connected
	INFO_CPU_TEMP: "9f9c635f-3321-4490-9bae-c58572125012",	// Device internal temperature
	INFO_CPU_LOAD: "9f9c635f-3321-4490-9bae-c58572125013",	// Device CPU load
	INFO_MEMORY: "9f9c635f-3321-4490-9bae-c58572125014",	// Device memory usage
	INFO_UPTIME: "9f9c635f-3321-4490-9bae-c58572125015",	// Device up time
	INFO_BATTERY: "9f9c635f-3321-4490-9bae-c58572125016",	// Battery state
	INFO_CPU_USAGE: "9f9c635f-3321-4490-9bae-c58572125017",	// Device CPU Usage
}

bleVal =	{
	CMD_LOCATE_UNAVAILABLE: "unavailable",	// Can't flash LED
	CMD_LOCATE_AVAILABLE: "available",		// Can flash LED
	CMD_LOCATE_TOGGLE: "toggle",			// Toggle flashing

	WIFI_CREDS_MAX_LENGTH: "20",				// Max number of characters in SSID & password entries (set in device)
	BLE_MTU_PREF: 300,						// Prefered MTU (set in device)
}


